---
title: "About"
description: "All About Fons"
date: "2021-01-10"
type: "about"
layout: "single"
categories:
tags:
blurb: "Just a city boy, born and raised in South Detroit"
recentposts: 5
recentprojects: 5
photo: "/images/avatar.png"
cardheaderimage: "/images/default.jpg" #optional: default solid color if unset
cardbackground: "#263238" #optional: card background color; only shows when no image specified
---

#### Education
> tralalalala
>2015    MA, Wine Appreciation, University of Caledonia
>2013    BA, Painting with Condiments, University of Caledonia

#### Publications
> Big 'ol book of condiment paintings (2015)
> Distincive Properties of Caledonia Wine. Journal of Wine Tasting. (2015)

#### Places I've worked
>Claessen