---
title: Eerste Post
date: 2022-01-12T21:33:07.659Z
publishdate: 2022-01-12T21:33:07.683Z
description: Dit is een eerste post
draft: false
url: "{{slug}}"
categories:
  - verhalen
Tags:
  - verhaal
cardthumbimage: "/images/default.jpg" #optional: default solid color if unset
cardheaderimage: "/images/default.jpg" #optional: default solid color if unset
cardbackground: "#263238" #optional: card background color; only shows when no image specified
#cardtitlecolor: "#fafafa" #optional: can be changed to make text visible over card image
"author":
    name: "Fons Claessen"
    description: "Writer of stuff"
    website: "http://example.com/"
    email: "firstname@example.com"
    twitter: "https://twitter.com/"
    github: "https://github.com/"
    image: "/images/avatar-64x64.png"
---

**Dit is een eerste Post**


# **Dit is een H1 regel**