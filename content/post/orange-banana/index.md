---
title: "Orange banana"
description: "Second post with Hugo website engine"
date: "2022-01-06"
categories:
    - "post"
tags:
    - "meta"
cardthumbimage: "/images/default.jpg" #optional: default solid color if unset
cardheaderimage: "/images/default.jpg" #optional: default solid color if unset
cardbackground: "#263238" #optional: card background color; only shows when no image specified
#cardtitlecolor: "#fafafa" #optional: can be changed to make text visible over card image
"author":
    name: "Fons Claessen"
    description: "Writer of this stuff"
    website: "http://hugo.rubenwebdesign.nl"
    email: "fonsclaessenbs@gmail.com"
    github: "https://github.com/"
    image: "/images/avatar-64x64.png"
draft: false
---

# LIVE PREVIEW IS AWSOME!  Dit is nieuwe tekst

[Hi, I'm a link!](https://en.wikipedia.org/wiki/Markdown)

Ready to see an image?

![I'm an image!](./images/Orange-and-banana.jpeg)

<style>
mark{
    color:red;
    background:black
}
</style>

<mark>Hallo!</mark>


Link Attributes:
![alt text](./images/Orange-and-banana.jpeg){.center}

<!-- More common in Hugo -->

hier nog een plaatje

{{< figure src="./images/Orange-and-banana.jpeg" caption="dit is een caption,mooi hoor" >}}

> Hi there, I'm a blockquote.

Look, some *italics!* So emphatic.

That's a **bold** statement, to be sure!

## Now for a bulleted list (Heading 2)

* list item 1
* another list item
* and another list item

You have to draw the line somewhere:

---

And sometimes --- you wouldn't believe it --- an em dash (*This isn't supported by all markdown processors.*)

### A fancy *numbered* list (Heading 3)

1. Numbered list item
1. Another numbered list item
1. Yup, *another* numbered list item
1. Note that I can use a "1." for every list item, and it automatically numbers the list for me!

Can you spot the `{inline code}` surrounded by backticks?

What about the triple backtick **code block** below?

``` html
<p>
  They've got me surrounded!
</p>
```

What if we put some of this in a beautiful table?

| Element | Description |
| ------- | ------------|
| *       | lists; italics |
| ``      | inline code |
| etc...  | you get the idea |
