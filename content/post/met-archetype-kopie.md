---
title: met archetype kopie
date: 2022-01-13T10:12:51.392Z
publishdate: 2022-01-13T10:12:51.406Z
description: de archetype voor post in folder gezet vanuit template
draft: false
url: "{{slug}}"
categories:
  - verhalen
Tags:
  - verhaal
cardthumbimage: "/images/default.jpg" #optional: default solid color if unset
cardheaderimage: "/images/default.jpg" #optional: default solid color if unset
cardbackground: "#263238" #optional: card background color; only shows when no image specified
#cardtitlecolor: "#fafafa" #optional: can be changed to make text visible over card image
"author":
    name: "Fons Claessen"
    description: "Writer of stuff"
    website: "http://example.com/"
    email: "firstname@example.com"
    twitter: "https://twitter.com/"
    github: "https://github.com/"
    image: "/images/avatar-64x64.png"
---
Test post